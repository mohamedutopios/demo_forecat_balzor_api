﻿using MyBlazor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyBlazor.Services
{
    public class HttpWeatherForecastService : IWeatherForecastService
    {


        private readonly HttpClient client;


        public HttpWeatherForecastService(HttpClient client)
        {

            this.client = client;

        }


        public async Task<WeatherForecast[]> GetForecastAsync(DateTime startDate)
        {

            var reponse = await client.GetAsync("WeatherForecast");

            if (reponse.IsSuccessStatusCode)
            {
                var jsonData = await reponse.Content.ReadAsStringAsync();
                var jsonOptions = new System.Text.Json.JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                };
                return System.Text.Json.JsonSerializer.Deserialize<WeatherForecast[]>(jsonData, jsonOptions);
            }


            return new WeatherForecast[0];

        }
    }
}
