﻿using MyBlazor.Model;
using System;
using System.Threading.Tasks;

namespace MyBlazor.Services
{
    public interface IWeatherForecastService
    {
        Task<WeatherForecast[]> GetForecastAsync(DateTime startDate);
    }
}